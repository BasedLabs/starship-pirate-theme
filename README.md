# starship-pirate-theme

## My own edits using the starship shell prompt
# Starship Outside of a git repo
![starship-outside-of-a-git-repo](https://user-images.githubusercontent.com/76766272/129909339-78994c86-4e78-4b17-b8ea-442bfd3bddd6.png)

# Starship inside of a git repo
![starship-inside-of-a-git-repo](https://user-images.githubusercontent.com/76766272/129909430-f85c8fc0-cdeb-45f0-b354-8b8d1ec16443.png)

# Full terminal view
![2021-08-18_07-58](https://user-images.githubusercontent.com/76766272/129922777-90f673cd-c620-4022-8167-5160673289b9.png)

